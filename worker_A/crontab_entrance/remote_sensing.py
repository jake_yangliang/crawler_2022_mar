import sys
from worker_A.issue_volume_checker import *
from journal_article_crawler import *
from volume_article_table_CRUD import *


def update_volume_now_last_year(driver, start_year, end_year):

    # 選擇 寫入/更新  預設：開始年份 結束年份 issue url
    volumes_url = newest_volume_url_get(driver, start_year, end_year)
    get_issue_href_in_volume(driver, volumes_url)


# 可設定 journal_volume_article.volume_sn_count() , journal_volume.link_article_count() 的 年份範圍 預設 今年跟去年
# return 兩邊count()不同的 issue網址
def update_tables_count(driver, start_year, end_year):

    # 拿到該年分範圍內的 issue url
    need_compare_article_count_issue_url = check_range_worker(start_year, end_year)

    need_check = []

    for issue_url in need_compare_article_count_issue_url:

        # 拿到 journal_volume.link.article_count
        journal_volume_count = int(journal_volume_link_article_count_result(issue_url))

        # 拿到 issue_url 的 journal_volume.sn
        volume_sn = issue_volume_sn_get(issue_url)

        # 拿到 journal_volume_article.volume_sn_count()
        article_volume_sn_count = int(journal_volume_article_count_result(volume_sn))

        # 若 journal_volume.article_count > journal_volume_article.volume_sn_count()
        # 代表有遺漏掉最新的 article 要把這issue 加入列表
        if journal_volume_count > article_volume_sn_count:
            need_check.append(issue_url)

        if journal_volume_count == article_volume_sn_count:
            update_journal_issue_update_datetime(issue_url)

    # issue urls
    return need_check


# 接收issue url 爬取 issue內 article url
def article_crawler(driver, need_check_issue_urls):

    for issue_url in need_check_issue_urls:

        driver.get(issue_url)
        # 拿到 issue 內 所有article url
        article_urls_in_issue = article_url_in_issue_page(driver, issue_url)

        # 拿到 issue url 的 journal_volume.sn
        volume_sn = issue_volume_sn_get(issue_url)
        # 單一文章網址
        for article_url in article_urls_in_issue:

            check_result = check_article_in_article_db(article_url)

            if check_result == 'insert':

                # 寫入基本資料
                insert_db_article_only_url(article_url, volume_sn, 16)

            if check_result == 'exist':

                # 更新 journal_volume_article.update_datetime
                update_article_info(article_url)


# 撈出 journal_volume_article.update_datetime is null 的 article_link
def keep_searching_null_article(driver):

    wait_to_crawler_article_urls = get_null_update_time_article_link(16)

    for article_url in wait_to_crawler_article_urls:

        article_info = remote_sensing_article_crawler(driver, article_url)

        update_article_crawler_result(article_info)


#  入口
def go(driver, start_year, end_year):

    """
    1. 更新 起始年 結束年 的 volume.issue
    2. 比對 journal_volume.article_count  & journal_volume_article.volume_sn_count() 只要不同 撈出 issue_url
    3. 進入issue url 爬取 article_url 寫入 journal_volume_article.article_link/create_datetime/volume_sn/journal_sn
    4. if journal_volume_article.update_datetime = Null 撈出 journal_volume_article.article_link 進行爬取
    """

    # 更新符合年份的 volume issue
    update_volume_now_last_year(driver, start_year, end_year)
    # 拿到年份內 兩個table.count() 不同的issue url
    need_check_urls = update_tables_count(driver, start_year, end_year)
    # 進入issue 爬取 issue 內的 article_url 基本資料 寫入/更新 journal_volume_article
    article_crawler(driver, need_check_urls)
    # 撈出 journal_volume_article.update_datetime is null 的 article_link 80筆 並爬取
    keep_searching_null_article(driver)
    driver.close()


if __name__ == '__main__':

    Driver = selenium_()
    # 輸入範圍年份
    if len(sys.argv) > 1:

        start_year = int(sys.argv[1])
        end_year = int(sys.argv[2])
        if end_year > start_year:
            print('範圍錯誤:結束年份 > 開始年份 請重新輸入')

        else:
            go(Driver, start_year, end_year)

    else:

        # 預設年份 今年 去年
        this_year = get_newest_year_in_journal_volume(16)
        last_year = this_year - 1

        go(Driver, last_year, this_year)



