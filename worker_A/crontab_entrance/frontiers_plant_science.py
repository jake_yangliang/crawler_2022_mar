from volume_article_table_CRUD import *
import sys
from worker_A.issue_volume_checker import *
from journal_article_crawler import *


def go(driver, start_point_year, end_point_year):

	"""
	1.每次確認 有沒有新的volume
	2.確認這次要更新的volume 年份範圍
	3.進入volume_url 比對兩邊 article_count 數量:

		if journal_volume.volume_url.article_count != journal_volume_url.volume_sn_count():
			進入 volume url 逐一判斷 article url 是否存在 :
				不存在：寫入資料
				存在： update_datetime 更新

			結束後 更新 journal_volume_article.update_datetime , article_count

		if journal_volume.volume_url.article_count == journal_volume_url.volume_sn_count():

			更新 journal_volume.volume_url.update_datetime ,article_count

	4.撈出 journal_volume_article.update_datetime/.title == null 的 article_link
	5.逐一爬取那些article_link 並 insert/update into journal_volume_article
	"""

	# 確認網路上 newest volume 並 update/insert journal_volume.link
	check_newest_volume_exist(driver)
	# 更新完畢後 確認這次要更新的 volume 範圍 並拿到該年份 volume的urls
	volume_urls_in_year_range = get_year_range_volume_links(4, start_point_year, end_point_year)

	need_check_url = []

	# 逐一比對 journal_volume.link.article_count 跟 journal_volume_article.volume_sn_count() 數量
	for volume_url in volume_urls_in_year_range:

		# journal_volume.article_count
		journal_volume_count = int(journal_volume_link_article_count_result(volume_url))
		# volume_sn
		volume_sn = issue_volume_sn_get(volume_url)
		# journal_volume_article.volume_sn_count()
		article_volume_sn_count = int(journal_volume_article_count_result(volume_sn))

		# 兩者 != 代表 有新article 需要重爬 volume
		if journal_volume_count != article_volume_sn_count:
			print('count不一樣:', volume_url)
			# volume內所有article_link 都 insert/update journal_volume_article.article_link
			get_all_article_link_in_page(driver, volume_url)
			# volume 處理完成 update journal_volume.update_datetime
			article_count_update(journal_volume_count, volume_url)

		# 兩者 == 代表 volume無新article
		if journal_volume_count == article_volume_sn_count:
			print('count一樣:', volume_url)
			# update journal_volume.volume_link.update_datetime
			update_journal_issue_update_datetime(volume_url)
			# update journal_volume.article_count
			article_count_update(journal_volume_count, volume_url)

	# 撈出 title,update_datetime 為空的 article_link
	need_crawl_articles = get_null_update_time_article_link(4)

	print('journal volume article內 空 title url數量：', len(need_crawl_articles))
	# 逐一爬取上述 article 內容
	for article_link in need_crawl_articles:

		journal = get_article_info(driver, article_link)

		if journal == 'no abstract':
			continue
		if journal == 'pass':
			continue

		# 判斷 article insert/update
		check_result = check_article_in_article_db(journal['article_link'])

		if check_result == 'insert':
			print('insert:', article_link)
			insert_db_article(journal)
		if check_result == 'exist':
			print('update', article_link)
			update_article_crawler_result(journal)


if __name__ == '__main__':

	Driver = selenium_()

	# 輸入範圍年份
	if len(sys.argv) > 1:

		start_year = int(sys.argv[1])
		end_year = int(sys.argv[2])

		if end_year > start_year:
			print('範圍錯誤:結束年份 > 開始年份 請重新輸入')

		else:
			go(Driver, start_year, end_year)
			Driver.close()

	else:
		print('預設年份： 去年 ～ 今年')
		# 預設年份 今年(journal_volume.journal.sn.year_max())
		this_year = int(get_newest_year_in_journal_volume(4))
		last_year = this_year - 1

		go(Driver, last_year, this_year)
		Driver.close()

