from tools import *
from selenium.webdriver.common.by import By
import re
from journal_volume_CRUD import *


# remote sensing 主頁 爬取  volume href
def newest_volume_url_get(driver, start_year, end_year):

	driver.get('https://www.mdpi.com/journal/remotesensing')

	# 爬取所有期數網址
	volume_href = driver.find_elements(By.CSS_SELECTOR, 'div[class="journal-browser-volumes "] li[class="side-menu-li"] a')

	volume_hrefs = []

	for href_text in volume_href:

		volume_text = href_text.text
		volume_year = volume_text.split('(')[-1]
		year = int(re.sub(r"\D", "", volume_year))

		if end_year >= year >= start_year:

			href = href_text.get_attribute('href')
			volume_hrefs.append(href)
			print(year)

	print(volume_hrefs)

	return volume_hrefs


# 接收 volume href ,進入 volume href 拿到 issue href, 將issue info 寫入 journal_volume
def get_issue_href_in_volume(driver, volume_hrefs):

	href_class = []

	issue_url_class = ''
	# 進入 volume
	for volume_url in volume_hrefs:

		driver.get(volume_url)
		# 確認 volume
		volume_checker = int(volume_url.split('/')[-1])

		issues = []
		# > page 7 網頁標籤有改
		if volume_checker > 7:
			# 拿到 issue url
			issue_url_class = driver.find_elements(By.CSS_SELECTOR, 'div[class="issue-cover"] div:last-child a')

		# < page 7
		if volume_checker < 8:
			# 拿到 issue url
			issue_url_class = driver.find_elements(By.CSS_SELECTOR, "div[class='ul-spaced'] ul li a")

		# 拿到issue url
		for issue_url in issue_url_class:
			issue_href = issue_url.get_attribute('href')
			issues.append(issue_href)

		# 將 issue url 寫入 journal_volume
		for url in issues:

			if get_issue_info(driver, url) == 'page updating':
				continue

			year, issue, article_count = get_issue_info(driver, url)

			journal_exist_check = newest_issue_check(url)
			# 有的話 寫入
			if journal_exist_check == 'insert':
				insert_journal_newest_issue(16, url, volume_checker, 'issue', issue, year, article_count)
			# 沒有的話 更新
			if journal_exist_check == 'update':
				print(year, issue, 'issue url exist!')
				update_journal_newest_issue(url, article_count)


# remote_sensing:issue info 爬蟲 ,爬取 year, issue, article_count
def get_issue_info(driver, issue_url):
	# issue 內容
	driver.get(issue_url)

	try:
		volume_class = driver.find_element(By.CSS_SELECTOR, "div[class='breadcrumb__element']:nth-child(4) a").text
		issue_class = driver.find_element(By.CSS_SELECTOR, "div[class='breadcrumb__element']:nth-child(5) a").text
		# issue volume
		issue = re.sub(r"\D", "", issue_class)
		volume = int(re.sub(r"\D", "", volume_class))

		# 確認issue內文章數量
		article_count_class = driver.find_element(By.CSS_SELECTOR, "div[class='columns large-12 medium-12 small-12']").text
		article_count = article_count_class.split('-')[1]
		# issue 年份
		year = driver.find_element(By.CSS_SELECTOR, 'div[class="color-grey-dark"] b').text

		return year, issue, article_count

	except:

		return 'page updating'



