from tools import *
from selenium.webdriver.common.by import By
import re
from journal_volume_CRUD import *


# 確認最新 volume , update/insert volume info
def check_newest_volume_exist(driver):

	print('check all volume start')

	url_head = "https://www.ncbi.nlm.nih.gov/pmc/?term=(%22Front+Plant+Sci%22%5BJournal%5D)+AND+"
	url_end = "%5BVolume%5D"

	volume = 1
	# 直到網頁出現'無資料'即停止
	while True:

		volume_url = url_head + str(volume) + url_end

		driver.get(volume_url)
		# 判斷是否顯示無資料 出現 就停止
		try:
			check_str = driver.find_element(By.CSS_SELECTOR, 'span[class="icon"]').text
			print('check all volume over')
			break

		except:
			# 確認 journal_volume.volume_url 是否存在
			check_result = newest_issue_check(volume_url)

			# journal_sn, article_count, year = fps_volume_url_info(driver, volume_url)
			article_count = driver.find_element(By.CSS_SELECTOR, 'meta[name="ncbi_resultcount"]').get_attribute(
				'content')
			journal_sn = 4
			year = driver.find_element(By.CSS_SELECTOR, 'span[class="citation-publication-date"]').text
			year = int(re.sub(r"\D", "", year))

			if check_result == 'insert':
				insert_journal_newest_issue(journal_sn, volume_url, volume, None, None, year, article_count)

			if check_result == 'update':
				update_journal_newest_issue(volume_url, article_count)
			# 下一期
			volume += 1

