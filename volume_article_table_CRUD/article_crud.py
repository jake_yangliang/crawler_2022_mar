from tools import *
from datetime import datetime
from itertools import chain


# 單純寫入
# journal_volume_article.link
# journal_volume_article.journal_sn
# journal_volume_article.volume_sn
# journal_volume_article.create_datetime
# 等待發動


def insert_db_article_only_url(url, volume_sn, journal_sn):
	conn, cursor = db_set()

	create_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

	sql = "insert into journal_volume_article (`article_link`,`volume_sn`,`journal_sn`,`create_datetime`) values ('%s','%s','%s',now())" % (
		url, volume_sn, journal_sn)

	cursor.execute(sql)
	conn.commit()
	conn.close()


# 確認 article_url 是否存在於db
def check_article_in_article_db(article_url):
	conn, cursor = db_set()

	sql = "select article_link from journal_volume_article where `article_link`='%s'" % article_url

	cursor.execute(sql)
	result = cursor.fetchone()
	if result is None:

		return "insert"
	else:

		return "exist"


# update article info
def update_article_info(article_url):
	conn, cursor = db_set()

	sql = "update journal_volume set `update_datetime`=now() where `link`='%s' " % (
		article_url)

	print('updating : ' + article_url)

	cursor.execute(sql)
	conn.commit()
	conn.close()


# insert article info
def insert_db_article(journal):
	conn, cursor = db_set()

	update_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

	journal['update_datetime'] = update_datetime

	print('Inserting : ' + journal['doi'])

	cols = ", ".join('`{}`'.format(k) for k in journal.keys())

	sql = ('insert into journal_volume_article '
		   '(`abstract`,'
		   ' `title`, '
		   '`article_link`,'
		   ' `authors`, '
		   '`location`, '
		   '`year`,'
		   ' `doi`, '
		   '`keywords`, '
		   '`journal_sn`, '
		   '`update_datetime`) '
		   'values'
		   ' (%(abstract)s, '
		   '%(title)s, '
		   '%(article_link)s, '
		   '%(authors)s, '
		   '%(location)s, '
		   '%(year)s, '
		   '%(doi)s, '
		   '%(keywords)s, '
		   '%(journal_sn)s, '
		   '%(update_datetime)s) ')

	cursor.execute(sql, journal)

	conn.commit()
	conn.close()


# 每次80筆 撈出journal_volume_article.update_datetime = NULL  的 journal_volume_article.article_link
def get_null_update_time_article_link(journal_sn):
	conn, cursor = db_set()

	# 確認 title update_datetime is null
	sql = ("select article_link from journal_volume_article where `update_datetime` is null and `title` is null and `journal_sn`=%s") % journal_sn

	cursor.execute(sql)
	result = cursor.fetchall()
	result = list(chain.from_iterable(result))

	return result


def update_article_crawler_result(journal):

	conn, cursor = db_set()

	update_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	journal['update_datetime'] = update_datetime

	sql = "update journal_volume_article set `update_datetime`=now(), `abstract`= %(abstract)s, `title`=%(title)s, `authors`=%(authors)s, `location`=%(location)s, `year`=%(year)s, `doi`=%(doi)s, `keywords`=%(keywords)s,`update_datetime`=%(update_datetime)s where `article_link`=%(article_link)s"

	cursor.execute(sql, journal)
	conn.commit()
	conn.close()
