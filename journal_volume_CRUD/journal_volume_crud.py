from tools import *
from itertools import chain


# 接收 issue 的資料 寫入 journal_volume
def insert_journal_newest_issue(journal_sn, url, volume, sub_type, sub_type_value, year, article_count):
	conn, cursor = db_set()

	sql = "insert into journal_volume (`journal_sn`, `link` , `volume`, `sub_type` ,`sub_type_value` ,`year` , `link_article_count`, `create_datetime`) values (%s,%s,%s,%s,%s,%s,%s,now())"

	cursor.execute(sql, (journal_sn, url, volume, sub_type, sub_type_value, year, article_count))
	conn.commit()


# 接收 issue url 確認 issue 是否存在於 journal_volume
def newest_issue_check(issue_url):
	conn, cursor = db_set()

	sql = "select link FROM journal_volume WHERE link='%s' " % issue_url

	cursor.execute(sql)
	result = cursor.fetchone()

	if result is None:
		return "insert"
	else:
		return 'update'


# 只需更新的話 接收 issue 結果 將該 issue/volume url 結果 update
def update_journal_newest_issue(url, article_count=None):
	conn, cursor = db_set()

	sql = "update journal_volume set `update_datetime`=now() ,`link_article_count`='%s' where `link`='%s' " % (
		article_count, url)

	cursor.execute(sql)
	conn.commit()


# update issue update_datetime
def update_journal_issue_update_datetime(volume_url):
	conn, cursor = db_set()

	sql = "update journal_volume set `update_datetime`=now() where `link`='%s' " % volume_url

	cursor.execute(sql)
	conn.commit()


# 接收開始年分 限制更新開始年份 若無 全更新
def check_range_worker(start_year, end_year):
	conn, cursor = db_set()

	sql = "select link FROM journal_volume WHERE `year` between %s and %s " % (start_year, end_year)

	cursor.execute(sql)

	result = cursor.fetchall()
	result = list(result)
	need_check_result = list(chain.from_iterable(result))

	return need_check_result


# 依照 volume的url 撈出 journal_volume.link_article_count 的數量
def journal_volume_link_article_count_result(volume_url):
	conn, cursor = db_set()
	# 撈出 volume_url 的 journal_volume.link_article_count
	journal_volume_count_sql = "select `link_article_count` from `journal_volume` where `link`='%s'" % volume_url

	cursor.execute(journal_volume_count_sql)
	journal_count_result = cursor.fetchone()[0]

	if journal_count_result is None:
		return 0

	else:
		return journal_count_result


# 接收issue的 url return: volume_sn
def issue_volume_sn_get(issue_url):
	conn, cursor = db_set()

	sql = "select sn from journal_volume where `link`='%s'" % issue_url

	cursor.execute(sql)

	volume_sn = cursor.fetchone()[0]

	return volume_sn


# 撈出journal_volume_article 內來自同一期的的資料總數
def journal_volume_article_count_result(volume_sn):
	conn, cursor = db_set()

	journal_volume_article_count_sql = "select * from `journal_volume_article` where `volume_sn`='%s'" % volume_sn

	cursor.execute(journal_volume_article_count_sql)
	result = cursor.fetchall()
	count_number = len(result)

	return count_number


# get journal_volume.year_max()
def get_newest_year_in_journal_volume(journal_sn):
	conn, cursor = db_set()

	journal_volume_article_count_sql = "select max(year) from `journal_volume` where `journal_sn`= %s" % journal_sn

	cursor.execute(journal_volume_article_count_sql)
	result = cursor.fetchone()[0]

	return result


# update journal_volume.article_count
def article_count_update(article_count, volume_url):
	conn, cursor = db_set()

	sql = "update journal_volume set `update_datetime`=now(), `link_article_count`= %s where `link`='%s' " % (
		article_count, volume_url)

	cursor.execute(sql)
	conn.commit()


# 拿到 range = start_year ~ end_year and journal_volume.journal_sn = 4 的 volume_urls
def get_year_range_volume_links(journal_sn, start_year, end_year):
    conn, cursor = db_set()

    sql = "select link from journal_volume where journal_sn=%s and year between %s and %s" % (journal_sn, start_year, end_year)

    cursor.execute(sql)
    result = cursor.fetchall()
    result = list(chain.from_iterable(result))

    return result
