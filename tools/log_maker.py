import time
from tools import *


def queue_log_maker(journal_id, volumes, url_counts, insert_status, e_msg=None):

    now_date = time.strftime("%Y-%m-%d")
    time_str = time.strftime("%Y-%m-%d %H:%M")

    path = "/home/jake_yang/journal_new/logs/" + now_date + " queue_log.txt"

    with open(path, 'a') as txt:

        logs = (
            "寫入時間: " + time_str
            + " // 寫入雜誌ID: " + str(journal_id)
            + " // 寫入期數/頁數(開始,結束): " + str(volumes)
            + " // 寫入數量: " + str(url_counts)
            + " // 寫入狀態: " + insert_status
            + "\n"
        )

        txt.write(logs)

        logs2 = ("錯誤訊息:" + str(e_msg).replace('\n', '').replace('\r', '') + "\n")

        txt.write(logs2)


def article_log_maker(journal_id, url_counts, insert_status, url=None, e_msg=None):

    now_date = time.strftime("%Y-%m-%d")
    time_str = time.strftime("%Y-%m-%d %H:%M")

    path = "/home/jake_yang/journal_new/logs/" + now_date + " article_log.txt"

    with open(path, 'a') as txt:

        logs = (
            "寫入時間: " + time_str
            + " // 寫入雜誌ID: " + str(journal_id)
            + " // 寫入數量: " + str(url_counts)
            + " // 寫入狀態: " + insert_status
            + " // 中斷網址: " + str(url)
            + "\n"
        )
        txt.write(logs)

        logs2 = ("錯誤訊息:" + str(e_msg).replace('\n', '').replace('\r', '') + "\n")

        txt.write(logs2)


def queue_clean_log_maker():

    conn, cursor = db_set()

    now_date = time.strftime("%Y-%m-%d")
    time_str = time.strftime("%Y-%m-%d %H:%M")

    command = "SELECT journal_db_status from journal_volume WHERE journal_db_status=1"
    cursor.execute(command)
    results = len(cursor.fetchall())

    path = "/home/jake_yang/journal_new/logs/" + now_date + " queue_clean_log.txt"

    with open(path, 'a') as txt:

        logs = "已在 " + time_str + " Delete掉 " + str(results) + "筆已寫入的資料" + "\n"

        txt.write(logs)

    conn.close()

