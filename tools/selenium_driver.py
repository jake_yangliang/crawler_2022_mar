import webdrivermanager
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdrivermanager.chrome import ChromeDriverManager
import platform




def selenium_():
    # selenium相關設定  headers/不讀取網頁圖片
    opt = Options()
    fh = ("Safari: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit/605.1.15 (KHTML, like Gecko) "
          "Version/13.0 Safari/605.1.15")
    opt.add_argument("user-agent={}".format(fh))
    opt.add_argument("--blink-settings=imagesEnabled=false")
    opt.add_argument('--no-sandbox')
    opt.add_argument('--disable-dev-shm-usage')
    opt.add_argument('--dns-prefetch-disable')
    opt.add_argument("window-size=1920,1080")
    # opt.add_argument('--headless')
    opt.add_experimental_option('excludeSwitches', ['enable-automation'])
    opt.add_argument('--profile-directory=Default')

    result = str(platform.system())

    if result == "Darwin":
        driver_apple = webdriver.Chrome(
            executable_path=r'/Users/yangkailiang/Desktop/Work/2022_journal_crawler/tools/chromedriver',
            chrome_options=opt)
        return driver_apple

    if result == "Windows":
        driver_windows = webdriver.Chrome(
            executable_path=r'D:\PycharmProjects\2022_journal_crawler\tools\chromedriver.exe', chrome_options=opt)
        return driver_windows




