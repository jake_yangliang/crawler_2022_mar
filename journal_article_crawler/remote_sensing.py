from selenium.webdriver.common.by import By
from tools import *


# remote sensing 單一網頁爬蟲
def remote_sensing_article_crawler(driver, url):

    journal = {}

    driver.get(url)

    # title
    title = driver.find_element(By.CSS_SELECTOR, "meta[name='dc.title']").get_attribute('content')

    # 作者
    authors_list = []
    authors_meta = driver.find_elements(By.CSS_SELECTOR, "meta[name='dc.creator']")
    for authors in authors_meta:
        author = authors.get_attribute('content')
        authors_list.append(author)

    # location
    location = driver.find_element(By.CSS_SELECTOR, "div[class='affiliation-name ']").text

    # 出版年分
    publish_date = driver.find_element(By.CSS_SELECTOR, "meta[name='dc.date']").get_attribute('content')[0:4]

    # DOI
    doi = driver.find_element(By.CSS_SELECTOR, "meta[name='dc.identifier']").get_attribute('content')

    # 概論
    abstract = ''
    abstract = driver.find_element(By.CSS_SELECTOR, "div[class='art-abstract in-tab hypothesis_container']").text

    # 關鍵字
    keywords = []
    keywords_class = driver.find_elements(By.CSS_SELECTOR, "div[class='art-keywords in-tab hypothesis_container'] a")
    for keyword in keywords_class:
        keywords.append(keyword.text)

    journal['title'] = title
    journal['article_link'] = url
    journal['authors'] = ', '.join(authors_list)
    journal['location'] = location
    journal['year'] = publish_date
    journal['doi'] = doi
    journal['abstract'] = abstract
    journal['journal_sn'] = 16
    journal['keywords'] = ', '.join(keywords)

    return journal


# issue 內 article url 爬蟲
def article_url_in_issue_page(driver, issue_url):

    driver.get(issue_url)

    urls = []

    # 確認issue內文章數量
    article_count_class = driver.find_element(By.CSS_SELECTOR,
                                              "div[class='columns large-12 medium-12 small-12']").text
    article_count = article_count_class.split('-')[1]
    # 比對 總文章數量 網頁上已載入的數量 不同的話 持續往下
    while True:

        href_class = driver.find_elements(By.CSS_SELECTOR, 'a[class="title-link"]')
        href_len = len(href_class)

        js = "var action=document.documentElement.scrollTop=100000000"
        driver.execute_script(js)
        # 數量一樣 停止
        if int(article_count) == int(href_len):
            break

    # 爬取所有連結網址
    href_class = driver.find_elements(By.CSS_SELECTOR, 'a[class="title-link"]')
    for article_url in href_class:
        urls.append(article_url.get_attribute('href'))

    return urls

