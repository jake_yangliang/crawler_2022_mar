from tools import selenium_
from journal_volume_CRUD import *
import time
from selenium.webdriver.common.by import By
from volume_article_table_CRUD import *
from selenium import webdriver


# 爬取單一 volume內article的相關資料並 insert/update journal_volume_article 準備爬取
def get_all_article_link_in_page(driver, volume_url):

	# 進入volume_url
	driver.get(volume_url)

	article_urls = []
	# 點擊下一頁 爬取所有文章urls
	while True:

		href_class = driver.find_elements(By.CSS_SELECTOR, 'div[class="title"] a[class="view"]')

		for href in href_class:
			url = href.get_attribute('href')
			article_urls.append(url)

		# 確認翻頁按鈕存在 不存在 代表無volume
		try:

			driver.find_element(By.CSS_SELECTOR, "a[class='active page_link next']").click()
			time.sleep(1)

		except:

			# 翻到最後一頁後 統計 所有文章數量
			article_count = len(article_urls)
			article_count_update(article_count, volume_url)

			# get journal_volume.volume_sn
			volume_sn = issue_volume_sn_get(volume_url)

			# 逐一將文章網址寫入 journal_volume.link 待爬取
			for article_url in article_urls:
				# 確認文章url是否存在於 journal_volume_article 內
				check_result = check_article_in_article_db(article_url)

				if check_result == 'insert':
					# 寫入基本資料
					print('insert:', article_url)
					insert_db_article_only_url(article_url, volume_sn, 4)

				if check_result == 'exist':
					# 更新 journal_volume_article.update_datetime
					update_article_info(article_url)
			break


# 單篇論文的info
def get_article_info(driver, article_url):

	journal = {'keywords': ''}

	driver.get(article_url)

	# title
	title = driver.find_element(By.CSS_SELECTOR, "meta[name='citation_title']").get_attribute('content')
	if 'Retraction:' in title:

		return 'pass'

	# author
	author = driver.find_element(By.CSS_SELECTOR, 'meta[name="citation_authors"]').get_attribute('content')

	# abstract

	abstract = ''

	try:
		abstract_class1 = driver.find_element(By.CSS_SELECTOR, "p[class='p p-first']")
		abstract = abstract_class1.text
	except:

		abstract_class2 = driver.find_element(By.CSS_SELECTOR, 'p[class="p p-first-last"]')
		abstract = abstract_class2.text

	# location
	location_class = driver.find_element(By.CSS_SELECTOR,
										 "div[class='togglers fm-copyright-license'] a[class='pmctoggle']:first-child").click()

	if driver.current_url != article_url:
		driver.back()

	location = driver.find_element(By.CSS_SELECTOR, "div[class='fm-affl']").text

	# publish date
	publish_date = driver.find_element(By.CSS_SELECTOR, "meta[name='citation_date']").get_attribute('content')

	# doi
	doi = driver.find_element(By.CSS_SELECTOR, "meta[name='citation_doi']").get_attribute('content')

	# keywords
	try:

		keywords = driver.find_element(By.CSS_SELECTOR, 'span[class="kwd-text"]').text.replace(' ', '').replace(
			'\n', ',').split(',')
		journal['keywords'] = ', '.join(keywords)

	except:

		journal['keywords'] = None

	journal['title'] = title
	journal['article_link'] = article_url
	journal['authors'] = author
	journal['location'] = location
	journal['year'] = publish_date
	journal['doi'] = doi
	journal['abstract'] = abstract
	journal['journal_sn'] = 4

	return journal
